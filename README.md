## Install instruction

Before install and connect any TDC board download and install this communication driver:

- http://www.ftdichip.com/Drivers/CDM/CDM21226_Setup.zip
- http://www.ftdichip.com/Drivers/D3XX/FTD3XXDriver_WHQLCertified_v1.2.0.6_Installer.zip

# Changelog

### 0.0.0.7

- Fixed an error that cause crash at connection

### 0.0.0.6

- Fixed multiple saving data in MeasurePackager

### 0.0.0.5

- Added Save Data in LZT Measure Pack

### 0.0.0.4

- Graphical FIX

### 0.0.0.3

- Deleted some unused buttons

### 0.0.0.2

- Added in LZT Packager a count meter

### 0.0.0.1

- Initial release.


